/**
D image library color struct.

Copyright: Denis Shelomovskij 2016

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dimage.color;


import std.algorithm; 
static import std.ascii;
import std.range; 
import std.traits; 
import std.meta; 
import std.math; 


struct RGB
{
	ubyte red, green, blue;
	alias r = red, g = green, b = blue;
}

struct BGR
{
	ubyte blue, green, red;
	alias b = blue, g = green, r = red;
}

struct Gray
{
	ubyte value;
	alias v = value;
}

struct Monochrome
{
	ubyte value;
	alias v = value;
	alias value this;
}


// FIXME: Needed?
Color fromHex(Color)(in char[3] hex)
if(isRGBColor!Color)
in { foreach(const ch; hex) assert(std.ascii.isHexDigit(ch)); }
body
{
	const Color color =
	{
		red    : [hex[0], hex[0]]._hexToUbyte(),
		green  : [hex[1], hex[1]]._hexToUbyte(),
		blue   : [hex[2], hex[2]]._hexToUbyte(),
	};
	return color;
}

Color fromHex(Color)(in char[6] hex)
if(isRGBColor!Color)
in { foreach(const ch; hex) assert(std.ascii.isHexDigit(ch)); }
body
{
	const Color color =
	{
		red    : hex[0 .. 2]._hexToUbyte(),
		green  : hex[2 .. 4]._hexToUbyte(),
		blue   : hex[4 .. 6]._hexToUbyte(),
	};
	return color;
}

Color gray(Color)(in ChannelType!Color value)
if(isRGBColor!Color)
{
	const Color color =
	{
		red    : value,
		green  : value,
		blue   : value,
	};
	return color;
}

Color gray(Color)(in real value)
if(isRGBColor!Color && isUnsigned!(ChannelType!Color))
{
	alias Channel = ChannelType!Color;
	const Color color =
	{
		red    : value.floatToIntegralChannel!Channel,
		green  : value.floatToIntegralChannel!Channel,
		blue   : value.floatToIntegralChannel!Channel,
	};
	return color;
}

private Channel floatToIntegralChannel(Channel)(in real value)
if(isUnsigned!(Channel))
in { assert(value >= 0 && value <= 1); }
body
{
	return cast(Channel) rndtol(value * Channel.max);
}

alias ChannelType(Color) = typeof(Color.tupleof[0]);

ChannelType!Color value(Color)(in Color color)
if(isRGBColor!Color)
{
	alias Channel = ChannelType!Color;
	static if(isIntegral!Channel)
		alias T = ulong;
	else
		alias T = real;
	const sum = cast(T) color.red + color.green + color.blue;
	return cast(Channel) _integralAverage(sum, 3uL);
}

Color interpolate(Color)(in Color color1, in Color color2, in real frac)
if(isColor!Color)
in { assert(frac >= 0 && frac <= 1); }
body
{
	static if(is(Color == struct))
	{
		Color avg;
		foreach(i, Channel; typeof(Color.tupleof))
		{
			avg.tupleof[i] = interpolateChannel(color1.tupleof[i], color2.tupleof[i], frac);
		}
		return avg;
	}
	else
	{
		return interpolateChannel(color1, color2, frac);
	}
}

private Channel interpolateChannel(Channel)(in Channel channel1, in Channel channel2, in real frac)
if(isColorChannel!Channel)
in { assert(frac >= 0 && frac <= 1); }
body
{
	const floatingResult = channel1 * (1 - frac) + channel2 * frac;
	static if(isIntegral!Channel)
		return cast(Channel) rndtol(floatingResult);
	else
		return floatingResult;
}

NewColor as(NewColor, Color)(in Color color)
if(isRGBColor!Color && isRGBColor!NewColor)
{
	const NewColor newColor =
	{
		red    : color.red,
		green  : color.green,
		blue   : color.blue,
	};
	return newColor;
}

enum isRGBColor(Color) = isColor!Color && __traits(compiles, (inout int = 0)
{
	auto c = Color.init;
	auto r = c.red;
	auto g = c.green;
	auto b = c.blue;
	static assert(is(typeof(r) == typeof(g)));
	static assert(is(typeof(r) == typeof(b)));
});

enum isColorChannel(Channel) = isIntegral!Channel || isFloatingPoint!Channel;

template isColor(Color)
{
	static if(is(Color == struct))
		enum isColor = allSatisfy!(isColorChannel, typeof(Color.tupleof));
	else
		enum isColor = isColorChannel!Color;
}

ElementType!R average(R)(auto ref R range)
if(isForwardRange!R && hasLength!R && isColor!(ElementType!R ))
{
	alias Color = ElementType!R;

	static if(is(Color == struct))
	{
		Color avg;
		foreach(i, Channel; typeof(Color.tupleof))
		{
			avg.tupleof[i] = range.averageChannel!(c => c.tupleof[i]);
		}
		return avg;
	}
	else
	{
		return range.averageChannel!(c => c);
	}
}

private auto averageChannel(alias mapFunc, R)(auto ref R range)
if(isForwardRange!R && hasLength!R && isColor!(ElementType!R))
{
	alias Channel = typeof(mapFunc(ElementType!R.init));

	static if(isIntegral!Channel && isUnsigned!Channel)
		alias T = ulong;
	else static if(isIntegral!Channel)
		alias T = long;
	else
		alias T = real;

	R r = range.save;
	const sum = r.map!(c => cast(T) mapFunc(c)).sum();
	static if(isIntegral!Channel)
		return cast(Channel) sum._integralAverage(cast(ulong) range.length);
	else
		return sum / range.length;
}

@safe pure nothrow @nogc:

private long _integralAverage(in long sum, in ulong count)
{
	if(sum >= 0)
		return cast(long) ((cast(ulong) sum + count / 2) / count);
	else if(sum != long.min)
		return -cast(long) ((cast(ulong) -sum + count / 2) / count);
	else
		return -cast(long) ((cast(ulong) long.max + 1uL + count / 2) / count);
}

private ulong _integralAverage(in ulong sum, in ulong count)
{
	return (sum + count / 2) / count;
}

unittest
{
	assert(fromHex!RGB("000") == RGB.init);
	assert(fromHex!RGB("123") == RGB(0x11, 0x22, 0x33));
	assert(fromHex!RGB("abc") == RGB(0xAA, 0xBB, 0xCC));
	assert(fromHex!RGB("FFF") == RGB(0xFF, 0xFF, 0xFF));

	assert(fromHex!RGB("123456") == RGB(0x12, 0x34, 0x56));
	assert(fromHex!RGB("abcdef") == RGB(0xAB, 0xCD, 0xEF));
	assert(fromHex!RGB("ABCDEF") == RGB(0xAB, 0xCD, 0xEF));
}

private ubyte _hexToUbyte(in char[2] hexDigits)
in { foreach(const ch; hexDigits) assert(std.ascii.isHexDigit(ch)); }
body
{
	return cast(ubyte) (16 * hexDigits[0]._hexValue + hexDigits[1]._hexValue);
}

unittest
{
	assert("00"._hexToUbyte == 0);
	assert("09"._hexToUbyte == 9);
	assert("0A"._hexToUbyte == 10);
	assert("0F"._hexToUbyte == 15);
	assert("10"._hexToUbyte == 16);
	assert("90"._hexToUbyte == 0x90);
	assert("FF"._hexToUbyte == 255);
}

private ubyte _hexValue(in char hexDigit)
in { assert(std.ascii.isHexDigit(hexDigit)); }
body
{
	return cast(ubyte) (hexDigit >= 'A' ? (hexDigit & 0x4F) - 'A' + 10 : hexDigit - '0');
}

unittest
{
	assert('0'._hexValue == 0);
	assert('9'._hexValue == 9);
	assert('A'._hexValue == 10);
	assert('F'._hexValue == 15);
	assert('a'._hexValue == 10);
	assert('f'._hexValue == 15);
}
