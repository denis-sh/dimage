/**
D image library drawing functions.

Copyright: Denis Shelomovskij 2016

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dimage.draw;


import std.algorithm;
import std.math;

import dimage.color;
import dimage.image;


@safe pure nothrow @nogc:

void drawSmoothThinLine(Color)(ref Image!Color image, in Color color,
	in real fromX, in real fromY,
	in real toX, in real toY) @trusted
in
{
	foreach(const coord; [fromX, fromY, toX, toY])
		assert(coord.isFinite);
}
body
{
	real startX = fromX,
		startY = fromY,
		endX = toX,
		endY = toY;

	if(max(fromX, toX) < 0 || min(fromX, toX) > image.width ||
		max(fromY, toY) < 0 || min(fromY, toY) > image.height)
		return;

	real xDelta = endX - startX;
	real yDelta = endY - startY;

	const horizontal = xDelta.abs >= yDelta.abs;

	if(horizontal ? startX > endX : startY > endY)
	{
		swap(startX, endX);
		swap(startY, endY);
		xDelta = -xDelta;
		yDelta = -yDelta;
	}

	const c = startX * yDelta - startY * xDelta;

	if(horizontal)
	{
		scope const yAt = (in real x) => (yDelta * x - c) / xDelta;

		foreach(const x; cast(size_t) max(startX, 0u) .. cast(size_t) min(endX.ceil + 1, image.width))
		{
			const y = yAt(x), y1 = y.floor, y2 = y.ceil;

			if(y2 < 0 || y1 >= image.height)
				continue;

			if(y1 == y2)
			{
				image[x, cast(size_t) y1] = color;
			}
			else
			{
				const frac = y2 - y;

				if(y1 >= 0)
				{
					Color* ptr = &image[x, cast(size_t) y1];
					*ptr = (*ptr).interpolate(color, frac);
				}

				if(y2 < image.height)
				{
					Color* ptr = &image[x, cast(size_t) y2];
					*ptr = (*ptr).interpolate(color, 1 - frac);
				}
			}
		}
	}
	else
	{
		scope const xAt = (in real y) => (xDelta * y + c) / yDelta;

		foreach(const y; cast(size_t) max(startY, 0u) .. cast(size_t) min(endY.ceil + 1, image.height))
		{
			const x = xAt(y), x1 = x.floor, x2 = x.ceil;

			if(x2 < 0 || x1 >= image.width)
				continue;

			if(x1 == x2)
			{
				image[cast(size_t) x1, y] = color;
			}
			else
			{
				const frac = x2 - x;

				if(x1 >= 0)
				{
					Color* ptr = &image[cast(size_t) x1, y];
					*ptr = (*ptr).interpolate(color, frac);
				}

				if(x2 < image.width)
				{
					Color* ptr = &image[cast(size_t) x2, y];
					*ptr = (*ptr).interpolate(color, 1 - frac);
				}
			}
		}
	}
}

void drawThinLine(Color)(ref Image!Color image, in Color color,
	in long fromColumnIndex, in long fromRowIndex,
	in long toColumnIndex, in long toRowIndex) @trusted
{
	// FIXME: Incomplete draft implementation.

	long fromX = fromColumnIndex,
		minY = fromRowIndex,
		toX = toColumnIndex,
		maxY = toRowIndex;

	if(minY > maxY)
	{
		swap(fromX, toX);
		swap(minY, maxY);
	}

	if(minY > image.height || min(fromX, toX) > image.width)
		return;

	if(minY == maxY)
	{
		if(fromX > toX)
			swap(fromX, toX);
		const rowStartX = cast(size_t) max(0u, fromX);
		const rowEndX = cast(size_t) min(image.width, toX + 1);
		image[rowStartX .. rowEndX, cast(size_t) minY][] = color;
		return;
	}

	const yDelta = maxY - minY;
	const xDelta = toX - fromX;

	const size_t
		fromRawIndex = cast(size_t) max(0u, minY),
		endRawIndex = min(cast(ulong) maxY + 1u, image.height);
	const xk = xDelta > 0 ? 1 : -1;

	long xAt(in long y)
	{
		return fromX + (xDelta * (y - minY) + xk * yDelta / 2) / yDelta;
	}

	if(maxY - minY <= abs(fromX - toX))
	{
		foreach(const y; fromRawIndex .. endRawIndex)
		{
			long segmentFromX = xAt(y);
			long nextX__ = xAt(y + 1);
			long segmentToX = y == endRawIndex - 1 ? toX : xAt(y + 1) - xk;
			if(xDelta < 0)
				swap(segmentFromX, segmentToX);

			if(segmentFromX >= image.width || segmentToX < 0u)
				continue;

			const rowStartX = cast(size_t) max(0u, segmentFromX);
			const rowEndX = cast(size_t) min(image.width, segmentToX + 1);
			image[rowStartX .. rowEndX, y][] = color;
		}
	}
	else
	{
		foreach(const y; fromRawIndex .. endRawIndex)
		{
			image[cast(size_t) xAt(y), y] = color;
		}
	}
}

unittest
{
	import dimage.color;


	void checkLine(ubyte n)(in int x1, in int y1, in int x2, in int y2,
		in ubyte[n * n] expectedRedChannel) @trusted nothrow @nogc
	{
		auto img = Image!RGB(n, n);
		img.drawThinLine(fromHex!RGB("FFF"), x1, y1, x2, y2);
		const x= img.dataArray;
		foreach(const i, const n; expectedRedChannel)
			assert(img.dataArray[i].red == (n ? 255 : 0));
	}

	version(none)
	checkLine!3(0, 0, 3, 1,
	[
		1, 1, 0,
		0, 0, 1,
		0, 0, 0,
	]);

	// Check diagonals

	version(none)
	foreach(const x1; -2 .. 1)
	foreach(const y1; -2 .. 1)
		checkLine!2(x1, y1, 0, 0, [1, 0,  0, 0]);

	foreach(const x2; 1 .. 3)
	foreach(const y2; 1 .. 3)
		checkLine!2(1, 1, x2, y2, [0, 0,  0, 1]);

	version(none)
	foreach(const xy1; -2 .. 1)
	foreach(const xy2; 1 .. 3)
		checkLine!2(xy1, xy1, xy2, xy2, [1, 0,  0, 1]);

	checkLine!2(1, 0, 0, 1,
	[
		0, 1,
		1, 0,
	]);

	checkLine!4(1, 1, 2, 2,
	[
		0, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 0,
	]);

	checkLine!4(0, 0, 4, 4,
	[
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	]);

	// Check other angles

	checkLine!3(0, 0, 1, 2,
	[
		1, 0, 0,
		0, 1, 0,
		0, 1, 0,
	]);

	checkLine!3(1, 0, 2, 3,
	[
		0, 1, 0,
		0, 1, 0,
		0, 0, 1,
	]);

	if(0) checkLine!3(0, 1, 3, 2,
	[
		0, 0, 0,
		1, 1, 0,
		0, 0, 1,
	]);

	checkLine!4(1, 0, 2, 3,
	[
		0, 1, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 1, 0,
	]);

	checkLine!5(2, 0, 3, 4,
	[
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
		0, 0, 0, 1, 0,
	]);

	checkLine!5(2, 0, 1, 4,
	[
		0, 0, 1, 0, 0,
		0, 0, 1, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 1, 0, 0, 0,
	]);

	checkLine!8(1, 0, 3, 6,
	[
		0, 1, 0, 0, 0, 0, 0, 0,
		0, 1, 0, 0, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
	]);

	checkLine!8(1, 0, 3, 7,
	[
		0, 1, 0, 0, 0, 0, 0, 0,
		0, 1, 0, 0, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0, 0,
	]);
}
