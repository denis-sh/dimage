/**
D image library image struct.

Copyright: Denis Shelomovskij 2016

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dimage.image;


//import core.stdc.stdlib;
import std.algorithm; // min, max, equal
import std.math;

import dimage.color;

@safe pure nothrow @nogc:

// FIXME: Can't be safe because of immutable mutation
// Disable immutable creation?

struct Image(Color)
if(isColor!Color)
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		size_t _width, _height,
			_stride;
		void* _data;
		bool _ownData; // Note: Can't be const to be able to assign Image.init
	}

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	@disable this(this);

	this(in size_t width, in size_t height) inout @trusted
	{
		this(width, height, true);
	}

	this(in size_t width, in size_t height, in Color initialColor) inout @trusted
	{
		this(width, height, true, initialColor);
	}

	this(in size_t width, in size_t height, in bool initializeData) inout @system
	{
		this(width, height, initializeData, Color.init);
	}

	private this(in size_t width, in size_t height, in bool initializeData, in Color initialColor) inout @system
	{
		const stride = width * Color.sizeof;
		const bytes = stride * height;
		auto data = bytes ? cast(Color*) _imageDataAlloc(stride * height) : null;
		if(bytes && initializeData)
			data[0 .. width * height] = initialColor;
		this(width, height, stride, cast(inout) data, bytes != 0);
	}

	this(in size_t width, in size_t height, in size_t stride, inout void* data, in bool ownData) inout @system
	{
		_width = width;
		_height = height;
		_stride = stride;
		_data = data;
		_ownData = ownData;
	}

	~this() @trusted
	{
		if(_ownData)
			_imageDataFree(data);
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	size_t width() const { return _width; }
	size_t height() const { return _height; }
	size_t stride() const { return _stride; }
	size_t padding() const { return stride - width * Color.sizeof; }
	size_t pixelCount() const { return width * height; }
	inout(void)* data() inout @system { return _data; }

	// FIXME: Name
	inout(Color)[] dataArray() inout @system
	in { assert(!padding || height <= 1); }
	body
	{ return (cast(inout Color*) data)[0 .. pixelCount]; }

	Image dup() const @trusted
	{
		auto copy = Image(width, height, false);
		copyTo(copy);
		return copy;
	}

	immutable(Image) idup() const @trusted
	{
		return cast(immutable) dup;
	}

	inout(Image) reference() inout @system
	{
		return inout Image(width, height, stride, data, false);
	}

	// NOTE: Can't be `inout` because of language limitations.
	auto byPixelColor() @system
	{
		return ImageByPixelColor!Color(width, padding, 0, 0, pixelCount, cast(Color*) data);
	}

	// NOTE: `const` overload as language limitations workaround.
	auto byPixelColor() const @system
	{
		return ImageByPixelColor!(const Color)(width, padding, 0, 0, pixelCount, cast(const Color*) data);
	}

	// NOTE: Can't be `inout` because of language limitations.
	auto byPixel() @system
	{
		return ImageByPixel!Color(width, padding, pixelCount, ImagePixel!Color(0, 0, cast(Color*) data));
	}

	// NOTE: `const` overload as language limitations workaround.
	auto byPixel() const @system
	{
		return ImageByPixel!(const Color)(width, padding, pixelCount, ImagePixel!(const Color)(0, 0, cast(const Color*) data));
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	inout(Color)* rowStart(in size_t index) inout @system
	{
		return cast(inout Color*) (data + index * stride);
	}

	inout(Color)[] row(in size_t index) inout @system
	{
		return rowStart(index)[0 .. width];
	}

	void clear(in Color color = Color.init) @trusted
	{
		if(!padding)
			dataArray[] = color;
		else foreach(const rowIndex; 0u .. height)
			row(rowIndex)[] = color;
	}

	Color getExtendedPixelColor(in real x, in real y) const @trusted
	in { assert(x.isFinite && y.isFinite); }
	body
	{
		const adjustedX = x < 0 ? 0 : min(x, width - 1u);
		const adjustedY = y < 0 ? 0 : min(y, height - 1u);

		const columnIndex = cast(size_t) adjustedX;
		const rowIndex = cast(size_t) adjustedY;
		const columnFraction = adjustedX - columnIndex;
		const rowFraction = adjustedY - rowIndex;

		if(columnFraction || rowFraction)
		{
			if(!columnFraction)
			{
				return interpolate
				(
					this[columnIndex, rowIndex],
					this[columnIndex, rowIndex + 1u],
					rowFraction
				);
			}
			else if(!rowFraction)
			{
				const row = row(rowIndex);
				return interpolate
				(
					row[columnIndex],
					row[columnIndex + 1u],
					columnFraction
				);
			}
			else
			{
				const row1 = row(rowIndex);
				const row2 = row(rowIndex + 1u);

				const row1Color = interpolate
				(
					row1[columnIndex],
					row1[columnIndex + 1u],
					columnFraction
				);

				const row2Color = interpolate
				(
					row2[columnIndex],
					row2[columnIndex + 1u],
					columnFraction
				);

				return interpolate
				(
					row1Color,
					row2Color,
					rowFraction
				);
			}
		}
		else
		{
			return this[columnIndex, rowIndex];
		}
	}

	Color getExtendedPixelColor(in long columnIndex, in long rowIndex) const
	{
		const _columnIndex = columnIndex < 0 ? 0u :
			cast(size_t) min(cast(ulong) columnIndex, width - 1u);
		const _rowIndex = rowIndex < 0 ? 0u :
			cast(size_t) min(cast(ulong) rowIndex, height - 1u);
		return this[_columnIndex, _rowIndex];
	}

	void copyTo(ref Image other) const @trusted
	in { assert(other.width == width && other.height == height); }
	body
	{
		if(!padding && other.stride == stride)
		{
			other.dataArray[] = dataArray[];
		}
		else foreach(const rowIndex; 0 .. height)
		{
			other.row(rowIndex)[] = row(rowIndex);
		}
	}

	Image roughDownsampled(in size_t horizontalScale, in size_t verticalScale) const
	{
		return roughResized(width / horizontalScale, height / verticalScale);
	}

	Image roughResized(in size_t newWidth, in size_t newHeight) const @trusted
	in
	{
		assert(newWidth && newHeight);
		assert(newWidth <= width && newHeight <= height);
	}
	body
	{
		auto res = Image(newWidth, newHeight, false);

		auto resPixel = cast(Color*) res.data;
		foreach(const resRowIndex; 0 .. res.height)
		{
			const rowIndex = height * resRowIndex / res.height;
			const nextRowIndex = height * (resRowIndex + 1) / res.height;
			foreach(const resColumnIndex; 0 .. res.width)
			{
				const columnIndex = width * resColumnIndex / res.width;
				const nextColumnIndex = width * (resColumnIndex + 1) / res.width;
				auto pixels = (cast() this)
				[
					columnIndex .. nextColumnIndex,
					rowIndex .. nextRowIndex
				].byPixelColor;
				*resPixel++ = pixels.average;
			}
		}

		return res;
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	bool opEquals(in Image other) const @trusted
	{
		return width == other.width && height == other.height &&
			equal(byPixelColor, other.byPixelColor);
	}

	bool opEquals(const ref Image other) const @trusted
	{
		return width == other.width && height == other.height &&
			equal(byPixelColor, other.byPixelColor);
	}

	size_t opDollar(size_t dim : 0)() const
	{ return width; }

	size_t opDollar(size_t dim : 1)() const
	{ return height; }

	ImageIndicesRange opSlice(size_t dim)(in size_t from, in size_t to) const
	{ return ImageIndicesRange(from, to); }

	ref inout(Color) opIndex(in size_t columnIndex, in size_t rowIndex) inout @trusted
	in { assert(columnIndex < width && rowIndex < height); }
	body
	{
		return rowStart(rowIndex)[columnIndex];
	}

	inout(Image) opIndex(in ImageIndicesRange columnRange, in ImageIndicesRange rowRange) inout @system
	in
	{
		with(columnRange) assert(from <= to && to <= width);
		with(rowRange) assert(from <= to && to <= height);
	}
	body
	{
		const sliceWidth = columnRange.to - columnRange.from;
		const sliceHeight = rowRange.to - rowRange.from;
		inout sliceData = &opIndex(columnRange.from, rowRange.from);
		return inout Image(sliceWidth, sliceHeight, stride, sliceData, false);
	}

	inout(Image) opIndex(in size_t columnIndex, in ImageIndicesRange rowRange) inout @system
	in
	{
		assert(columnIndex < width);
		with(rowRange) assert(from <= to && to <= height);
	}
	body
	{
		return opIndex(ImageIndicesRange(columnIndex, columnIndex + 1), rowRange);
	}

	inout(Color)[] opIndex(in ImageIndicesRange columnRange, in size_t rowIndex) inout @system
	in
	{
		with(columnRange) assert(from <= to && to <= width);
		assert(rowIndex < height);
	}
	body
	{
		const sliceWidth = columnRange.to - columnRange.from;
		inout sliceData = &opIndex(columnRange.from, rowIndex);
		return sliceData[0 .. sliceWidth];
	}
}

struct ImageByPixelColor(Color)
{	
pure nothrow @nogc:

	private
	{
		size_t _imageWidth, _imagePadding,
			_columnIndex, _rowIndex, _length;
		Color* _pixel;
	}

	size_t columnIndex() const
	{ return _columnIndex; }

	size_t rowIndex() const
	{ return _rowIndex; }

	bool empty() const
	{ return !_length; }

	size_t length() const
	{ return _length; }

	ref inout(Color) front() inout
	in { assert(!empty); }
	body
	{
		return *_pixel;
	}

	void popFront() @system
	in { assert(!empty); }
	body
	{
		--_length;
		++_pixel;
		++_columnIndex;
		if(_columnIndex == _imageWidth)
		{
			_columnIndex = 0;
			++_rowIndex;
			_pixel = cast(Color*)(cast(void*) _pixel + _imagePadding);

			if(empty)
				return;
		}
	}

	inout(ImageByPixelColor) save() inout @system
	{
		return this;
	}
}

struct ImagePixel(Color)
{
	size_t columnIndex, rowIndex;
	private Color* _color;

	alias x = columnIndex;
	alias y = rowIndex;

	ref inout(Color) color() inout @system
	{ return *_color; }
}

struct ImageByPixel(Color)
{	
pure nothrow @nogc:

	private
	{
		size_t _imageWidth, _imagePadding, _length;
		ImagePixel!Color _pixel;
	}

	bool empty() const
	{ return !_length; }

	size_t length() const
	{ return _length; }

	inout(ImagePixel!Color) front() inout
	in { assert(!empty); }
	body
	{
		return _pixel;
	}

	void popFront() @system
	in { assert(!empty); }
	body
	{
		--_length;
		++_pixel._color;
		++_pixel.columnIndex;
		if(_pixel.columnIndex == _imageWidth)
		{
			_pixel.columnIndex = 0;
			++_pixel.rowIndex;
			_pixel._color = cast(Color*)(cast(void*) _pixel._color + _imagePadding);

			if(empty)
				return;
		}
	}

	inout(ImageByPixel) save() inout @system
	{
		return this;
	}
}

private struct ImageIndicesRange
{
	size_t from, to;
}

package extern(C) @system
{
	pragma(mangle, "malloc") void* _imageDataAlloc(size_t bytes);
	pragma(mangle, "free") void _imageDataFree(void* imageData);
}

@trusted unittest
{
	enum w = 2, h = 3;
	auto img = Image!RGB(w, h);
	with(img)
	{
		assert(width == w);
		assert(height == h);
		assert(stride == w * RGB.sizeof);
		assert(!padding);
		assert(pixelCount == w * h);
		assert(data);
		assert(_ownData);
		assert(dataArray is cast(RGB[]) data[0 .. w * h * RGB.sizeof]);

		assert(rowStart(0) == data);
		assert(row(0) is dataArray[0 .. w]);
		assert(rowStart(1) == row(1).ptr);
		assert(row(2) is dataArray[$ - w .. $]);
	}
	assert(&img[0, 0] == img.data);
	assert(img[0, 0] == RGB.init);
	foreach(const ubyte i, ref color; img.dataArray)
		color = RGB(i, 0, 0);
	assert(img.getExtendedPixelColor(1, -1) == RGB(1, 0, 0));

	assert(img.getExtendedPixelColor(0.5, 0) == RGB(0, 0, 0));
	assert(img.getExtendedPixelColor(0.5, 1) == RGB((2 + 3) / 2, 0, 0));
	assert(img.getExtendedPixelColor(0.5, 2) == RGB((4 + 5) / 2, 0, 0));
	assert(img.getExtendedPixelColor(0.5, 0.5) == RGB((1 + 2 + 3) / 4, 0, 0));
	assert(img.getExtendedPixelColor(0.5, 1.5) == RGB((2 + 3 + 4 + 5) / 4, 0, 0));

	assert(img.reference.tupleof[0 .. $ - 1] == img.tupleof[0 .. $ - 1]);
	assert(!img.reference._ownData);

	void assertIsCopy()(const auto ref Image!RGB copyImage) nothrow @nogc
	{
		assert(copyImage.tupleof[0 .. $ - 2] == img.tupleof[0 .. $ - 2]);
		assert(copyImage.data != img.data);
		assert(copyImage._ownData);
		assert(copyImage.dataArray == img.dataArray);
	}

	assertIsCopy(img.dup);
	static assert(is(typeof((cast(const) img).dup()) == Image!RGB));
	assertIsCopy(img.idup);
	{
		auto imgCopy = Image!RGB(w, h);
		img.copyTo(imgCopy);
		assertIsCopy(imgCopy);
	}
	{
		import std.range: isForwardRange, hasLength;

		ubyte i = 0;
		foreach(ref color; img.byPixelColor)
			assert(&color == &img.dataArray[i++]);
		i = 0;
		foreach(ref color; (cast(const) img).byPixelColor)
			assert(&color == &img.dataArray[i++]);
		i = 0;
		foreach(pixel; img.byPixel)
		{
			assert(pixel.columnIndex == i % img.width);
			assert(pixel.rowIndex == i / img.width);
			assert(&pixel.color() == &img.dataArray[i++]);
		}
		i = 0;
		foreach(pixel; (cast(const) img).byPixel)
		{
			assert(pixel.columnIndex == i % img.width);
			assert(pixel.rowIndex == i / img.width);
			assert(&pixel.color() == &img.dataArray[i++]);
		}

		auto r = img.byPixelColor;
		static assert(isForwardRange!(typeof(r)));
		static assert(hasLength!(typeof(r)));
		assert(r.length == img.pixelCount);
		assert(r.save is r);
		assert((cast(const) r).save is r);
	}

	{
		auto imgFullSlice = img[0 .. $, 0 .. $];
		assert(imgFullSlice.tupleof[0 .. $ - 1] == img.tupleof[0 .. $ - 1]);
		assert(!imgFullSlice._ownData);
	}

	auto img2 = Image!RGB(11, 12);
	{
		auto imgSlice = img2[2 .. 7, 3 .. 5];
		with(imgSlice)
		{
			assert(width == 5);
			assert(height == 2);
			assert(stride == img2.stride);
			assert(imgSlice.padding == (11 - 5) * RGB.sizeof);
			assert(!_ownData);
		}
		assert(&imgSlice[0, 0] == &img2[2, 3]);
		assert(&imgSlice[4, 1] == &img2[6, 4]);
		assert(img2[2 .. 7, 3] is imgSlice.row(0));
		{
			auto r = imgSlice.byPixelColor;
			assert(&r.front() == &imgSlice[0, 0]);
			foreach(_; 0 .. 4) r.popFront();
			assert(&r.front() == &imgSlice[4, 0]);
			r.popFront();
			assert(&r.front() == &imgSlice[0, 1]);
			foreach(_; 0 .. imgSlice.width) r.popFront();
			assert(r.empty);
		}
	}
	{
		auto imgColumn = img2[2, 3 .. 6];
		with(imgColumn)
		{
			assert(width == 1);
			assert(height == 3);
			assert(stride == img2.stride);
			assert(imgColumn.padding == (11 - 1) * RGB.sizeof);
			assert(!_ownData);
		}
		assert(&imgColumn[0, 0] == &img2[2, 3]);
		assert(&imgColumn[0, 2] == &img2[2, 5]);
	}
	img2 = Image!RGB.init;


	img = Image!RGB.init;
}

@trusted unittest
{
	auto img4x4 = Image!byte(4, 4);
	foreach(const i, const n; cast(ubyte[])
	[
		1, 2, 5, 6,
		3, 4, 7, 8,
		2, 0, 8, 0,
		0, 2, 0, 0,
	])
		img4x4.dataArray[i] = n;

	const img2x2 = img4x4.roughDownsampled(2, 2);
	foreach(const i, const n; cast(ubyte[])
	[
		3, 7,
		1, 2,
	])
		assert(img2x2.dataArray[i] == n);

	const img1x1 = img2x2.roughDownsampled(2, 2);
	assert(img1x1[0, 0] == 3);
	assert(img4x4.roughResized(1, 1) == img1x1);
	assert(img4x4.roughDownsampled(4, 4) == img1x1);
	assert(img4x4.roughDownsampled(3, 3) == img1x1);
}
