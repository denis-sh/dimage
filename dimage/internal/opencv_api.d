/**
D image library OpenCV C++ API.

Copyright: Denis Shelomovskij 2016

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dimage.internal.opencv_api;


nothrow @nogc:


enum
{
	CV_CN_MAX    = 512,
	CV_CN_SHIFT  = 3,
	CV_DEPTH_MAX = 1 << CV_CN_SHIFT,

	CV_MAT_DEPTH_MASK      = CV_DEPTH_MAX - 1,
}

int CV_MAT_DEPTH(in int flags) @safe pure { return flags & CV_MAT_DEPTH_MASK; }

enum uint CV_MAT_CN_MASK = (CV_CN_MAX - 1) << CV_CN_SHIFT;

int CV_MAT_CN(in int flags) @safe pure
{
	return ((flags & CV_MAT_CN_MASK) >> CV_CN_SHIFT) + 1;
}

int CV_ELEM_SIZE1(in int type) @safe pure
{
	return (((size_t.sizeof << 28) | 0x8442211) >> CV_MAT_DEPTH(type) * 4) & 15;
}

int CV_ELEM_SIZE(in int type) @safe pure
{
	return CV_MAT_CN(type) << ((((size_t.sizeof / 4 + 1) * 16384 | 0x3a50) >> CV_MAT_DEPTH(type) * 2) & 3);
}

enum CVType
{
	_8u, _8s, _16u, _16s, _32s, _32f, _64f,
	_8uc3 = 16,
}

enum InterpolationFlags
{
	nearest            = 0,
	linear             = 1,
	cubic              = 2,
	area               = 3,
	lanczos4           = 4,
	max                = 7,
	warpFillOutliers   = 8,
	warpInverseMap     = 16,
}

enum BorderTypes
{
	constant    = 0,
	replicate   = 1,
	reflect     = 2,
	wrap        = 3,
	reflect101  = 4,
	transparent = 5,

	default_    = reflect101,
	isolated    = 16,
}

enum DecompositionType
{
    lu       = 0,
    svd      = 1,
    eig      = 2,
    cholesky = 3,
    qr       = 4,
    normal   = 16
};

extern(C++, cv)
{
	int waitKey(int delay = 0);

	extern(C++, class) struct Point_(T)
	{
		T x, y;
	}

	alias Point2f = Point_!float;

	extern(C++, class) struct Size_(T)
	{
		T width, height;
	}

	alias Size2i = Size_!int;
	alias Size = Size2i;

	extern(C++, class) struct Scalar_(T)
	{
		T[4] val = 0;
	}

	alias Scalar = Scalar_!double;
}
