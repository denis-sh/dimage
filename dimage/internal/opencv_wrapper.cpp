/**
D image library OpenCV wrapper.

Copyright: Denis Shelomovskij 2016

License: Boost License 1.0.

Authors: Denis Shelomovskij
*/
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;


namespace cv_wrapper
{
#define in const

	struct MatData
	{
		int rows, cols, type;
		void* data;
		size_t step;
	};

	typedef MatData KnownSizeOutputMatData;

	typedef MatData (*AllocateMatData)(in int rows, in int cols, in int type);


	static Mat matFromData(const MatData matData)
	{
		return Mat(matData.rows, matData.cols, matData.type, matData.data, matData.step);
	}

	static const Mat outMatFromData(const MatData matData)
	{
		return matFromData(matData);
	}

	static void copyMatToData(in Mat &srcMat, MatData destMatData)
	{
		srcMat.copyTo(matFromData(destMatData));
	}


	MatData readImage(in AllocateMatData allocateMatData, in char *const path, in int flags)
	{
		Mat mat = imread(path, flags);
		MatData matData = allocateMatData(mat.rows, mat.cols, mat.type());
		copyMatToData(mat, matData);
		return matData;
	}

	void showImage(in char *const windowName, in MatData &image)
	{
		imshow(windowName, matFromData(image));
	}


	void resize(in MatData &image, KnownSizeOutputMatData &resizedImage,
		in int interpolation)
	{
		const Size size = Size(resizedImage.cols, resizedImage.rows);
		cv::resize(matFromData(image), outMatFromData(resizedImage), size, 0, 0, interpolation);
	}


	void canny(in MatData &image, KnownSizeOutputMatData &edges,
		in double threshold1, in double threshold2,
		in int apertureSize, in bool L2gradient)
	{
		Canny(matFromData(image), outMatFromData(edges), threshold1, threshold2, apertureSize, L2gradient);
	}

	void lsd(const MatData &image,
		in int _refine, in double _scale,
		in double _sigma_scale, in double _quant, in double _ang_th,
		in double _log_eps, in double _density_th, in int _n_bins,
		void (*makeLines)(void*, float(*const lines)[4], float *width, size_t), void* context)
	{
		Ptr<LineSegmentDetector> ls = createLineSegmentDetector(_refine, _scale,
			_sigma_scale, _quant, _ang_th,
			_log_eps, _density_th, _n_bins);
		std::vector<Vec4f> lines;
		std::vector<float> width;
		ls->detect(matFromData(image), lines, width);
		makeLines(context, reinterpret_cast<float(*)[4]>(&lines[0]), &width[0], lines.size());
	}

	void invert(
		in MatData &srcMatrix,
		KnownSizeOutputMatData &destMatrix,
		in int flags)
	{
		cv::invert(matFromData(srcMatrix), outMatFromData(destMatrix), flags);
	}

	void getPerspectiveTransform(
		in Point2f *const srcPoints,
		in Point2f *const destPoints,
		KnownSizeOutputMatData &transform)
	{
		copyMatToData(getPerspectiveTransform(srcPoints, destPoints), transform);
	}

	void warpPerspective(
		in MatData &srcImage,
		MatData &destImage,
		in MatData &transform,
		in Size &destSize,
		in int flags,
		in int borderMode,
		in Scalar& borderValue)
	{
		cv::warpPerspective(matFromData(srcImage),
			matFromData(destImage),
			matFromData(transform),
			destSize,
			flags,
			borderMode,
			borderValue);
	}
}
