/**
D image library OpenCV wrapper C++ API.

Copyright: Denis Shelomovskij 2016

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dimage.internal.opencv_wrapper_api;


import dimage.internal.opencv_api;


nothrow @nogc:


extern(C++, cv_wrapper):

struct MatData
{
	int rows, cols, type;
	void* data;
	size_t step;
};

alias AllocateMatData = MatData function(in int rows, in int cols, in int type);

alias KnownSizeOutputMatData = MatData;

enum ImageReadMode
{
	unchanged  = -1,
	grayscale  =  0,
	color      =  1,
	anydepth   =  2,
	anycolor   =  4,
}

MatData readImage(in AllocateMatData allocateMatData, in char* path, in int flags);

void showImage(in char* windowName, const ref MatData image);

void resize(const ref MatData image, ref KnownSizeOutputMatData resizedImage,
	in int interpolation);

void canny(const ref MatData image, ref KnownSizeOutputMatData edges,
	in double threshold1, in double threshold2,
	in int apertureSize, in bool L2gradient);

void lsd(const ref MatData image,
	in int _refine, in double _scale,
	in double _sigma_scale, in double _quant, in double _ang_th,
	in double _log_eps, in double _density_th, in int _n_bins,
	void function(void*, float[4]*, float*, const size_t) makeLines, void* context);

void invert(
	const ref MatData srcMatrix,
	ref KnownSizeOutputMatData destMatrix,
	in int flags);

void getPerspectiveTransform(
	const cv.Point2f* srcPoints,
	const cv.Point2f* destPoints,
	ref KnownSizeOutputMatData transform);

void warpPerspective(
	const ref MatData srcImage,
	ref MatData destImage,
	const ref MatData transform,
	const ref cv.Size destSize,
	in int flags,
	in int borderMode,
	const ref cv.Scalar borderValue);
