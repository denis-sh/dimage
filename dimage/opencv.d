/**
D image library OpenCV function wrappers.

Copyright: Denis Shelomovskij 2016

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dimage.opencv;


import core.exception;
import core.checkedint;

import std.algorithm;
import std.array;
import std.internal.cstring;
import std.range;
import std.traits;

import dimage.internal.opencv_api;
import dimage.internal.opencv_wrapper_api;
import dimage;


nothrow @nogc:


// OpenCV's High-level GUI function wrappers.
// ----------------------------------------------------------------------------------------------------

Image!Color readImage(Color)(in char[] path) @trusted
if(isRGBColor!Color)
{
	auto imageBGR = readImageData(path, ImageReadMode.color).asImage!BGR;
	static if(is(Color == BGR))
		return imageBGR;
	else
		return imageBGR.recoloredAs!Color;
}

Image!ubyte readImage(Color : ubyte)(in char[] path) @trusted
{
	return readImageData(path, ImageReadMode.grayscale).asImage!ubyte;
}

private MatData readImageData(in char[] path, in ImageReadMode mode) @trusted
{
	return cv_wrapper.readImage(&allocateMatDataImpl, path.tempCString, mode);
}


void showWindow(Color)(const ref Image!Color image, in char[] windowName, in bool waitKey) @trusted
{
	const imageMatData = image.asMatData;
	showImageData(windowName, imageMatData);
	if(waitKey)
		cv.waitKey();
}

private void showImageData(in char[] windowName, in MatData imageData) @trusted
{
	cv_wrapper.showImage(windowName.tempCString, imageData);
}


// OpenCV's Image processing function wrappers.
// ----------------------------------------------------------------------------------------------------

Image!T resize(T)(const ref Image!T image,
	in size_t resizedWidth,
	in size_t resizedHeight,
	in InterpolationFlags interpolation = InterpolationFlags.linear) @trusted
{
	auto resizedImage = Image!T(resizedWidth, resizedHeight, false);
	const imageMatData = image.asMatData;
	auto resizedImageMatData = resizedImage.asMatData;
	cv_wrapper.resize(imageMatData, resizedImageMatData, interpolation);
	return resizedImage;
}

Image!ubyte canny(const ref Image!ubyte image,
	in double threshold1, in double threshold2,
	in int apertureSize = 3, in bool L2gradient = false) @trusted
{
	auto edges = Image!ubyte(image.width, image.height, false);
	const imageMatData = image.asMatData;
	auto edgesMatData = edges.asMatData;
	cv_wrapper.canny(imageMatData, edgesMatData, threshold1, threshold2, apertureSize, L2gradient);
	return edges;
}


struct LSDSegment
{
	float x1, y1, x2, y2, width;
}

enum LSDRefinement
{
	none,
	standard,
	advanced
}

LSDSegment[] lsd(const ref Image!ubyte image,
	in LSDRefinement refinement = LSDRefinement.standard, in double scale = 0.8,
	in double sigmaScale = 0.6, in double quant = 2.0, in double angTh = 22.5,
	in double logEps = 0, in double densityTh = 0.7, in int nBins = 1024) @trusted
{
	extern(C++) void makeLines(void* context, float[4]* ptr, float* width, const size_t count) 
	{
		*cast(LSDSegment[]*) context = iota(0u, count)
			.map!(i => LSDSegment(ptr[i][0], ptr[i][1], ptr[i][2], ptr[i][3], width[i]))
			.array;
	}

	LSDSegment[] segs;
	const imageMatData = image.asMatData;
	cv_wrapper.lsd(imageMatData,
		refinement, scale,
		sigmaScale, quant, angTh,
		logEps, densityTh, nBins,
		&makeLines, &segs);
	return segs;
}


alias Matrix3x3 = double[3][3];

Matrix3x3 inverted(in Matrix3x3 matrix, in DecompositionType decompositionType = DecompositionType.lu) @trusted
{
	auto matrixMatData = matrix.asMatData;
	Matrix3x3 result = void;
	auto resultMatData = result.asMatData;
	cv_wrapper.invert(matrixMatData, resultMatData, decompositionType);
	return result;
}


alias Point2f = cv.Point2f;
alias Scalar = cv.Scalar;

Matrix3x3 getPerspectiveTransform(
	in Point2f[4] srcPoints,
	in Point2f[4] destPoints) @trusted
{
	Matrix3x3 transform = void;
	auto transformMatData = transform.asMatData;
	cv_wrapper.getPerspectiveTransform(srcPoints.ptr, destPoints.ptr, transformMatData);
	return transform;
}

Point2 perspectiveTransformed(Point2)(in Point2 point, in Matrix3x3 transform)
if(Point2.tupleof.length == 2 && isFloatingPoint!(typeof(Point2.x)) && isFloatingPoint!(typeof(Point2.y)))
{
	const x = point.x, y = point.y;
	alias m = transform;
	const kx = m[0][0] * x + m[0][1] * y + m[0][2];
	const ky = m[1][0] * x + m[1][1] * y + m[1][2];
	const k  = m[2][0] * x + m[2][1] * y + m[2][2];
	return Point2(kx / k, ky / k);
}

void warpPerspective(Color)(
	const ref Image!Color srcImage,
	ref Image!Color destImage,
	const ref Matrix3x3 transform,
	in InterpolationFlags flags = InterpolationFlags.linear,
	in BorderTypes borderMode = BorderTypes.constant,
	in Scalar borderValue = Scalar.init) @trusted
{
	const srcImageMatData = srcImage.asMatData;
	auto destImageMatData = destImage.asMatData;
	const transformMatData = transform.asMatData;
	const destSize = cv.Size(destImage.width.dimAsInt, destImage.height.dimAsInt);

	cv_wrapper.warpPerspective(
		srcImageMatData,
		destImageMatData,
		transformMatData,
		destSize,
		flags,
		borderMode,
		borderValue);
}

version(testOpenCV)
@safe unittest
{
	enum path = "test-image.png";

	Image!BGR image = readImage!BGR(path);
	version(showWindows)
		image.showWindow("Color image", true);

	Image!ubyte grayImage = readImage!ubyte(path);
	version(showWindows)
		grayImage.showWindow("Gray image", true);

	Image!BGR resized = image.resize(500, 100);
	version(showWindows)
		resized.showWindow("Resized", true);

	Image!ubyte edges = grayImage.canny(100, 200);
	version(showWindows)
		edges.showWindow("Edges", true);

	const lsdSegments = grayImage.lsd;
	version(showWindows)
	{
		import std.math;
		foreach(const segment; lsdSegments)
			image.drawThinLine(fromHex!BGR("F00"), segment.x1.rndtol, segment.y1.rndtol, segment.x2.rndtol, segment.y2.rndtol);
		image.showWindow("LSD segments", true);
	}

	{
		const Matrix3x3 identity = [[1, 0, 0], [0, 1, 0], [0, 0, 1]];
		assert(identity.inverted == identity);

		const Matrix3x3 translate_dx_3_dy_5 = [[1, 0, 3], [0, 1, 5], [0, 0, 1]];
		const Matrix3x3 translate_dx_min_3_dy_min_5 = [[1, 0, -3], [0, 1, -5], [0, 0, 1]];
		assert(translate_dx_3_dy_5.inverted == translate_dx_min_3_dy_min_5);
		assert(translate_dx_min_3_dy_min_5.inverted == translate_dx_3_dy_5);
	}

	{
		const real w = image.width, h = image.height;
		const cv.Point2f[4] srcPoints = [{0, 0}, {w, 0}, {w, h * 2}, {-w, h}];
		const cv.Point2f[4] destPoints = [{0, 0}, {w, 0}, {w, h}, {0, h}];
		const transform = getPerspectiveTransform(srcPoints, destPoints);

		auto warpImage = Image!BGR(image.width, image.height);
		image.warpPerspective(warpImage, transform);
		version(showWindows)
			warpImage.showWindow("Perspective transform", true);
	}
}


// Utility functions.
// ----------------------------------------------------------------------------------------------------

private:

Image!Color asImage(Color)(MatData matData)
in { assert(cvType!Color == matData.type); }
body
{
	return Image!Color(cast(size_t) matData.cols, cast(size_t) matData.rows, matData.step, matData.data, true);
}

inout(MatData) asMatData(Color)(inout ref Image!Color image)
{
	inout MatData matData =
	{
		rows: image.height.dimAsInt,
		cols: image.width.dimAsInt,
		type: cvType!Color,
		data: image.data,
		step: image.stride,
	};
	return matData;
}

inout(MatData) asMatData(T, size_t rows, size_t cols)(inout ref T[cols][rows] array)
{
	inout MatData matData =
	{
		rows: rows,
		cols: cols,
		type: cvType!T,
		data: array.ptr,
		step: T.sizeof * cols,
	};
	return matData;
}

template cvType(Color)
if(isColor!Color)
{
	static if(is(Color == ubyte))        enum cvType = CVType._8u;
	else static if(is(Color == Gray))    enum cvType = CVType._8u;
	else static if(is(Color == byte))    enum cvType = CVType._8s;
	else static if(is(Color == ushort))  enum cvType = CVType._16u;
	else static if(is(Color == short))   enum cvType = CVType._16s;
	else static if(is(Color == int))     enum cvType = CVType._32s;
	else static if(is(Color == float))   enum cvType = CVType._32f;
	else static if(is(Color == double))  enum cvType = CVType._64f;
	else static if(is(Color == BGR))     enum cvType = CVType._8uc3;
	else
		static assert("'" ~ Color.stringof ~ "' isn't supported as OpenCV type.");
}

int dimAsInt(in size_t dim) @safe
{
	if(dim > int.max)
		assert(0);
	return cast(int) dim;
}

extern(C++) MatData allocateMatDataImpl(in int rows, in int cols, in int type) @trusted
{
	if(rows < 0 || cols < 0)
		assert(0);

	const elementSize = CV_ELEM_SIZE(type);

	bool overflow;
	const bytes = rows
		.mulu(cols, overflow)
		.mulu(elementSize, overflow);
	if(overflow)
		onOutOfMemoryError();

	void* data = _imageDataAlloc(bytes);
	if(!data)
		onOutOfMemoryError();

	return MatData(rows, cols, type, data, cols * elementSize);
}
