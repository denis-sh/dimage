﻿/**
D image library package module.

Copyright: Denis Shelomovskij 2016

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dimage;


public:
import dimage.size;
import dimage.color;
import dimage.image;
import dimage.transform;
import dimage.draw;
