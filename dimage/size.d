/**
D image library size struct.

Copyright: Denis Shelomovskij 2016

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dimage.size;


import std.algorithm; // min, max


@safe pure nothrow @nogc:

struct Size
{
pure nothrow @nogc:

	// Fields
	// ----------------------------------------------------------------------------------------------------

	long width, height;

	// Constants
	// ----------------------------------------------------------------------------------------------------

	enum Size empty = Size(0, 0);

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	this(in long width, in long height)
	{ this.width = width; this.height = height; }

	// Construction functions
	// ----------------------------------------------------------------------------------------------------

	static
	{
		Size square(in long length)
		{ return Size(length, length); }
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	const @property
	{
		long area()  { return width * height; }

		bool isEmpty() { return width == 0 && height == 0; }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	const
	{
		Size opBinary(string op : "+")(in Size sz2)
		{ return Size(width + sz2.width, height + sz2.height); }

		Size opBinary(string op : "-")(in Size sz2)
		{ return Size(width - sz2.width, height - sz2.height); }

		Size opBinary(string op : "&")(in Size sz2)
		{ return Size(min(width, sz2.width), min(height, sz2.height)); }

		Size opBinary(string op : "|")(in Size sz2)
		{ return Size(max(width, sz2.width), max(height, sz2.height)); }

		Size opBinary(string op : "*")(in long n)
		{ return Size(width * n, height * n); }

		Size opBinaryRight(string op : "*")(in long n)
		{ return opBinary!"*"(n); }

		Size opBinary(string op : "/")(in long n)
		{ return Size(width / n, height / n); }
	}
}
