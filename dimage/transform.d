/**
D image library transofrmation functions.

Copyright: Denis Shelomovskij 2016

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module dimage.transform;


import std.algorithm;
import std.functional;
import std.math;
import std.traits;

import dimage.color;
import dimage.image;


void flipVertically(Color)(ref Image!Color image) @trusted
{
	auto tmpImg = Image!Color(image.width, 1, false);
	auto tmpRow = tmpImg.row(0);
	foreach(const rowIdx; 0 .. image.height / 2u)
	{
		const otherRowIdx = image.height - 1u - rowIdx;
		tmpRow[] = image.row(rowIdx)[];
		image.row(rowIdx)[] = image.row(otherRowIdx)[];
		image.row(otherRowIdx)[] = tmpRow[];
	}
}

Image!Color verticallyFlipped(Color)(const auto ref Image!Color image) @safe
{
	auto res = image.dup;
	res.flipVertically;
	return res;
}

Image!Color transposed(Color)(const auto ref Image!Color image) @trusted
{
	auto res = Image!Color(image.height, image.width, false);
	foreach(const p; image.byPixel)
		res[p.y, p.x] = p.color;
	return res;
}

Image!Gray grayscaled(Color)(const auto ref Image!Color image) @trusted
if(isRGBColor!Color)
{
	auto res = Image!Gray(image.width, image.height, false);
	image.byPixelColor.map!(c => Gray(c.value)).copy(res.dataArray);
	return res;
}

Image!Monochrome monochromed(alias pred, Color)(const auto ref Image!Color image) @trusted
{
	auto res = Image!Monochrome(image.width, image.height, false);
	image.byPixelColor.map!(c => cast(Monochrome) !!unaryFun!pred(c)).copy(res.dataArray);
	return res;
}

auto recolored(alias func, Color)(const auto ref Image!Color image) @trusted
{
	alias NewColor = typeof(unaryFun!func(Color.init));
	auto res = Image!NewColor(image.width, image.height, false);
	image.byPixelColor.map!func.copy(res.dataArray);
	return res;
}

auto recoloredAs(NewColor, Color)(const auto ref Image!Color image) @trusted
if(isRGBColor!Color && isRGBColor!NewColor)
{
	return image.recolored!(c => c.as!NewColor);
}

auto recoloredAs(NewColor, Color : Gray)(const auto ref Image!Color image) @trusted
if(isRGBColor!NewColor)
{
	return image.recolored!(c => gray!NewColor(c.value));
}

auto recoloredAs(NewColor, Color : ubyte)(const auto ref Image!Color image) @trusted
if(isRGBColor!NewColor)
{
	return image.recolored!(c => gray!NewColor(c));
}

auto recoloredAs(NewColor, Color)(const auto ref Image!Color image) @trusted
if(isFloatingPoint!Color && isRGBColor!NewColor)
{
	return image.recolored!(c => gray!NewColor(c));
}

@trusted pure nothrow @nogc unittest
{
	auto img = Image!RGB(2, 2);
	foreach(const ubyte i, ref color; img.dataArray)
		color = RGB(i, cast(ubyte) (i * 3), 9);

	const grayscaled = img.grayscaled;
	foreach(const ubyte i, const gray; grayscaled.dataArray)
		assert(gray.value == (i + 1) / 3 + i + 3);

	const monochromed = img.monochromed!(c => c.red >= 2);
	foreach(const ubyte i, const monochrome; monochromed.dataArray)
		assert(monochrome.value == (i >= 2));

	const recolored = img.recolored!(c => RGB(c.g, 12, 255 - c.r));
	foreach(const ubyte i, const color; recolored.dataArray)
		assert(color == RGB(cast(ubyte) (i * 3), 12, 255 - i));

	const recoloredBGR = img.recoloredAs!BGR;
	foreach(const ubyte i, const color; recoloredBGR.dataArray)
		assert(color == BGR(9, cast(ubyte) (i * 3), i));
}
